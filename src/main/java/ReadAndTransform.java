import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

/**
 * Created by iutipikin on 5/16/16.
 */
public class ReadAndTransform {
    void readFilesFromDirectory (String path) {
        List<Path> filesToDelete = new ArrayList<>();
        if (!Files.exists(Paths.get("output.data"))){
            try {
                Files.createFile(Paths.get("output.data"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
        DateTimeFormatter formatOut = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        List<LocalDateTime> times = new ArrayList<>();
        LocalDateTime startTime = LocalDateTime.parse("2015-07-15 01:00:00.0", format);
        while (!startTime.isAfter(LocalDateTime.parse("2016-04-18 00:00:00.0", format))){
            times.add(startTime);
            startTime = startTime.plusHours(1);
        }
        String dateHeader = "";
        int k = 1;
        for (LocalDateTime time : times) {
            dateHeader = dateHeader.concat("\t" + k);
            k++;
        }
        dateHeader = dateHeader.concat("\n");
        System.out.println("DEBUG 1: " + dateHeader.split("\t").length);
        try (OutputStream out = new BufferedOutputStream(
                Files.newOutputStream(Paths.get("output.data"), CREATE, APPEND))) {
            byte[] data = dateHeader.getBytes();
            out.write(data,0,data.length);
        } catch (IOException x) {
            System.err.println(x.getMessage());
        }

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(path))) {
            for (Path entry: stream) {
                Map<LocalDateTime, Data> dats = new HashMap<>();
                List<String> file = Files.readAllLines(entry);
                file.forEach(line -> {
                    String[] idx = line.split("\t");
                    if (idx.length == 4) {
                        Data dat = new Data();
                        dat.setPlaceCode(Integer.valueOf(idx[0]));
                        dat.setDev_id(idx[1]);
                        dat.setTime(LocalDateTime.parse(idx[2], format));
                        dat.setValue(new Double(idx[3]));
                        dats.put(dat.getTime(), dat);
                    }
                });

                // Clear directory from partial files
//                if (dats.get(LocalDateTime.parse("2015-07-15 00:00:00.0", format)) == null || dats.get(LocalDateTime.parse("2016-04-18 00:00:00.0", format)) == null){
//                    filesToDelete.add(entry);
//                }

                System.out.println("filename: " + entry.getFileName());
                String output = dats.get(LocalDateTime.parse("2015-07-15 00:00:00.0", format)).getPlaceCode()+"\t";
                LocalDateTime start = LocalDateTime.parse("2015-07-15 01:00:00.0", format);

                List<Double> values = new ArrayList<>();

                while (!start.isAfter(LocalDateTime.parse("2016-04-18 00:00:00.0", format))){
                    int j=0;
                    if (dats.get(start) != null && dats.get(start.minusHours(1)) != null){
                        values.add(dats.get(start).getValue() - dats.get(start.minusHours(1)).getValue());
                        j++;
                    } else {
                        if (dats.get(start.minusHours(1)) != null){
                            LocalDateTime time = start;
                            int i = 0;
                            while(dats.get(time) == null){
                                time = time.plusHours(1);
                                i++;
                            }
                            Double avgStep = (dats.get(time).getValue() - dats.get(start.minusHours(1)).getValue())/i;
                            while (j<i){
                                Data avgData = new Data();
                                avgData.setValue(dats.get(start.minusHours(1)).getValue() + avgStep * (j+1));
                                avgData.setTime(start.plusHours(j));
                                dats.put(start.plusHours(j), avgData);
                                values.add(dats.get(start.plusHours(j)).getValue() - dats.get(start.plusHours(j - 1)).getValue());
                                j++;
                            }
                        }
                    }
                    start = start.plusHours(j);
                }
                for (Double value : values) {
                    output = output.concat(new BigDecimal(value)
                            .setScale(2, BigDecimal.ROUND_HALF_UP)
                            .doubleValue() + "\t");
                }
                System.out.println("DEBUG 2: " + output.split("\t").length);
                output = output.concat("\n");
                try (OutputStream out = new BufferedOutputStream(
                        Files.newOutputStream(Paths.get("output.data"), CREATE, APPEND))) {
                    byte[] data = output.getBytes();
                    out.write(data,0,data.length);
                } catch (IOException x) {
                    System.err.println(x.getMessage());
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.format("List of corrupt files (%d): \n",filesToDelete.size());
        filesToDelete.forEach( f -> System.out.println(f.getFileName()));
    }
}
