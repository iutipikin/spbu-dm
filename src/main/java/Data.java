import java.math.BigInteger;
import java.time.LocalDateTime;

/**
 * Created by iutipikin on 5/16/16.
 */
@lombok.Data
public class Data {
    Integer placeCode;
    String dev_id;
    LocalDateTime time;
    Double value;
}
